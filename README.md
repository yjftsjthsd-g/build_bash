# build_bash

Builds assorted versions of Bash.


## Use

Just go to the latest pipeline, find the version you want, download the tarball
(or download the zip and extract it), extract so the bash directory ends up in
`/opt`, and probably symlink like
```
sudo ln -svi /opt/bash-5.1/bin/bash /usr/local/bin/bash5.1
```


## License

*This* repo is under the unlicense, but of course this repo is just a bunch of
build scripts for GNU Bash, which is under the GNU GPL v2/3.

