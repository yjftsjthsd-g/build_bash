#!/bin/sh

VERSION="${1:?need version}"
MIRROR="${MIRROR:-https://mirrors.ocf.berkeley.edu/gnu/bash}"
URL="${MIRROR}/bash-${VERSION}.tar.gz"

wget "${URL}"

# TODO check checksum and/or signature
