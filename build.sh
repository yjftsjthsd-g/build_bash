#!/bin/sh

VERSION="${1:?need version}"
PREFIX="${PREFIX:-/opt/bash-${VERSION}}"
#CONFIGOPTS="${CONFIGOPTS:---foo"

echo "Building BASH version ${VERSION} for ${PREFIX}"

tar xf bash-${VERSION}.tar.gz
cd bash-${VERSION} && ./configure --prefix=${PREFIX} ${CONFIGOPTS} && make

